package com.example.tel_c3.cctvbdg.fragment;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.example.tel_c3.cctvbdg.R;

public class LogoFragment extends Fragment {

    WebView wb;
    ProgressBar pg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_logo_fragment, null);
        wb = (WebView)v.findViewById(R.id.webView);
        pg = (ProgressBar)v.findViewById(R.id.progressBar);
        wb.getSettings().setJavaScriptEnabled(true);
        wb.setWebViewClient(new WebViewClient());

        wb.getSettings().setDomStorageEnabled(true);
        wb.getSettings().setLoadWithOverviewMode(true);
        wb.getSettings().setUseWideViewPort(true);
        wb.getSettings().setSupportMultipleWindows(true);
        wb.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wb.setHorizontalScrollBarEnabled(false);
        wb.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        wb.getSettings().setAllowFileAccessFromFileURLs(true);
        wb.getSettings().setAllowUniversalAccessFromFileURLs(true);

        wb.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                pg.setVisibility(View.VISIBLE);
                pg.setProgress(newProgress);
                if (newProgress == 100) {
                    pg.setVisibility(View.GONE);
                }
            }
        });

        pg.setVisibility(View.VISIBLE);
        wb.loadUrl("https://www.facebook.com/fikrirazzaq/");
        return v;
    }

    @Override
    public void onPause() {
        wb.onPause();
        super.onPause();
    }
}

package com.example.tel_c3.cctvbdg.rest;


import com.example.tel_c3.cctvbdg.model.KameraResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by TEL-C 1 on 12/28/2016.
 */
public interface ApiInterfaceKamera {
    @GET("kordinat_kamera.php")
    Call<KameraResponse> getDataKamera();

}
